<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Formatter\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\OtpCode;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'role_id' => Role::whereRoleName('user')->first()->id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $otpCode = OtpCode::create([
            'user_id' => $user->id,
            'otp_code' => Str::upper(Str::random(6)),
            'expired_at' => Carbon::now()->addMinutes(5)
        ]);

        if ($user && $otpCode)
            return ResponseFormatter::success($user, 'Data added successfully!');
        else
            return ResponseFormatter::error(null, "Data added failed!", 404);
    }
}

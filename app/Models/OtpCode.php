<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class OtpCode extends Model
{
    use HasFactory, UseUuid;

    protected $table = 'otp_codes';

    protected $fillable = [
        'user_id',
        'otp_code',
        'expired_at'
    ];
}

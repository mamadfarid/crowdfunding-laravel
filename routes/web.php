<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware('CheckEmailVerification')->group( function (){
//     Route::get('route-1', [DashboardController::class, 'routeOne']);
// });

// Route::middleware(['CheckEmailVerification','CheckRole:admin'])->group( function (){
//     Route::get('route-2', [DashboardController::class, 'routeTwo']);
// });

// Auth::routes();

// Route::get('/home', [HomeController::class, 'index'])->name('home');

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Formatter\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::whereEmail($request->email)
            ->first()
            ->email_verified_at;

        if ( $user != null ) {

            if (!$token = Auth::attempt(['email' => $request->email, 'password' => $request->password]))
                return ResponseFormatter::error(null, "Login Failed!", 400);

            return ResponseFormatter::success(compact('token'), "Login successfully!", 200);

        } else {
            return ResponseFormatter::error(null, "Email has not been verified!", 400);
        }
    }
}

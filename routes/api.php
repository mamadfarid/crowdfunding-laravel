<?php

use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegenerateCodeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->prefix('auth')->group(function () {
    Route::post('register', [RegisterController::class, 'register']);
    Route::post('verification', [VerificationController::class, 'verification']);
    Route::post('regenerate-code', [RegenerateCodeController::class, 'regenerateCode']);
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LogoutController::class, 'logout']);
});

Route::middleware(['auth:api'])->namespace('Account')->prefix('account')->group(function () {
    Route::get('/', [AccountController::class, 'index']);
    Route::post('update', [AccountController::class, 'update']);
    Route::post('update-password', [AccountController::class, 'updatePassword']);
});

Route::get('user', [HomeController::class, 'index']);
